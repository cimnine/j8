# Presentation about the new features of Java 8, made with reveal.js 

This presentation covers
* Lambda
* Functional References
* Closures
* Streams

You can see this live on https://cimnine.ch/j8/
